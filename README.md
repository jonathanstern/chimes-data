# CHIMES Data

Welcome to the CHIMES Data repository. This contains the data files with the various reaction rate coefficients, photoionisation cross sections etc. that are needed for the CHIMES reaction network. 

For more information on CHIMES, please see the [main CHIMES home page](https://richings.bitbucket.io/chimes/home.html).

For more information on how to use CHIMES, please see the [CHIMES User Guide](https://richings.bitbucket.io/chimes/user_guide/index.html).
